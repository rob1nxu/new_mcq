(function () {
    "use strict";

    var app = angular.module("manuQuiz", []);

    app.controller("manuQuizCtrl", ["$http", manuQuizCtrl]);

    function manuQuizCtrl($http) {
        var self = this;

        self.quiz = {

        };

        self.userAns = {

            id: 0,
            value: "",
            comments: "",
            message: ""

        };


        self.initForm = function () {

            $http.get("/quizes")

                .then(function (result) {

                    console.log(result);
                    self.quiz = result.data;
                })

                .catch(function (e) {
                    console.log(e);

                });
        };


        self.initForm();

        self.submitQuiz = function () {

            self.userAns.id = self.quiz.id;

            $http.post("/answered", self.userAns)

                .then(function (result) { //result is basically from the server res
                    console.log(result);
                    if (result.data.isCorrect) { //isCorrect is from the res sent from server
                        self.userAns.message = "It's CORRECT !"; //self.finalanswer.message is for the index.html to inject the message
                    } else {
                        self.userAns.message = "TRY AGAIN !";
                    }
                }).catch(function (e) {
                    console.log(e);
                });







        };








    }
})();