/**
 * Server side code.
 */


console.log("hello");

"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 3050

app.use(express.static(__dirname + "/../client/"));

var myQuestions = [

    {
        id: 0,
        q: "who is wearing the no.9 jersey in ManU team?",
        ans1: { name: "lukaku", value: 1 },
        ans2: { name: "zaltan", value: 2 },
        ans3: { name: "anthony martial", value: 3 },
        corAns: 1
    },

    {
        id: 1,
        q: "who is the current manager of ManU?",
        ans1: { name: "louis van gaal", value: 1 },
        ans2: { name: "mourinho", value: 2 },
        ans3: { name: "david moyes", value: 3 },
        corAns: 2
    },

    {
        id: 2,
        q: "how much did ManU pay for lukaku?",
        ans1: { name: "100 pounds", value: 1 },
        ans2: { name: "70 pounds", value: 2 },
        ans3: { name: "75 pounds", value: 3 },
        corAns: 3
    }

];

app.get("/quizes", function (req, res) {

    var x = Math.random() * (4 - 1) + 1;
    var y = Math.floor(x);
    console.log(y);
    res.json(myQuestions[y - 1]);

});

app.post("/answered", function (req, res) {

    console.log("Received user object " + req.body);
    console.log("Received user object " + JSON.stringify(req.body));

    var incomingAns = req.body;

    var ansSheet = myQuestions[incomingAns.id];

    if (ansSheet.corAns == parseInt(incomingAns.value)) {

        console.log("correct!");
        incomingAns.isCorrect = true;
    }

    else {
        console.log("wrong!");
        incomingAns.isCorrect = false;
    }

    res.status(200).json(incomingAns);

});



app.use(function (req, res) {
    res.send("<h1>!!!! Page not found ! ! </h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});